## CrowdSec Interview #2

- Just for our listeners that may not have caught our first conversation or just don't recall, can you remind us what is CrowdSec?
- Since our last conversation Crowdsec from a community prespective has gotten a lot of notice.  Over 4000 stars on Github.  How has that community notice and contribution helped the project?
- Has Crowdsec also grown as a company?  Any new customers that you can talk about?
- I think we touched on it a little bit in our previous conversation but just as a reminder for our audience what is CowdSec Hub?
- How can the community contribute to CrowdSec Hub?
- Lets talk about the new release and some of the new features.  Can you give me a high level of what is in 1.2.1 release?
- For our listeners that may want to use CrowdSec in a public cloud enviornment how is that best accomplished?  Is CrowdSec ran as a service for public cloud?
- I noticed a blog post in November for integrating CrowdSec with Kubernetes.  This caught my eye because I started to think how I could use CrowdSec with OpenShift and I'm sure you did it in a similar way, what makes a CrowdSec implementation in Kubernetes work?  Are you doing side car containers or streaming data into the crowdsec pods to react to the data?
- CrowdSec console just went into public beta and based on your public roadmap it should be out in Q4 what should we expect from this?
- Anything on the roadmap that you are really excited about and would like feedback from the community on?
