# Sudo Show 42 - CrowdSec Revistied

[Episode 42](https://sudo.show/42)

## Show Notes

Philippe Humeau:
- [Twitter](https://twitter.com/philippe_humeau)
- [Opensource.com: New Open Source Project Crowdsources Internet Security](https://opensource.com/article/20/10/crowdsec)

Crowdsec:
- [Website](https://crowdsec.net/)

Contribute to Crowdsec:
- [CrowdSec Hub](https://hub.crowdsec.net/)
- [Github: Crowdsec](https://github.com/crowdsecurity/crowdsec)
- [Discourse](https://discourse.crowdsec.net/)
- [Gitter](https://gitter.im/crowdsec-project/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

Crowdsec on Social Media
- [Twitter](https://twitter.com/Crowd_Security)

Crowdsec Technical Documentation and Blog Posts
- [CrowdSec Log4J Threat Tracker](https://crowdsec.net/log4j-tracker/)
- [CrowdSec Log4J Senarios](https://hub.crowdsec.net/author/crowdsecurity/configurations/apache_log4j2_cve-2021-44228)
- [CrowdSec - Detect and block Log4J exploitation attempts](https://crowdsec.net/blog/detect-block-log4j-exploitation-attempts/)
- [CrowdSec on Kubernetes](https://crowdsec.net/blog/kubernetes-crowdsec-integration/)
- [CrowdSec on Public Cloud]()
- [CrowdSec at Scale](https://crowdsec.net/blog/multi-server-setup/)


Destination Linux and Sudo Show Links:
- [Destination Linux Network](https://destinationlinux.network)
- [Sudo Show Website](https://sudo.show)

Support the Show:
- [Sponsor: Bitwarden](https://bitwarden.com/dln)
- [Sponsor: Digital Ocean](https://do.co/dln)
- [Sudo Show Swag](https://sudo.show/swag)

Contact Us:
- [DLN Discourse](https://sudo.show/discuss)
- [Email Us!](mailto:contact@sudo.show)
- [Sudo Matrix Room](https://sudo.show/matrix)

Follow our Hosts:
- [Brandon's Website](https://open-tech.net)
- [Brandon's Twitter](https://twitter.com/dbrandonjohnson)


## Chapters
00:00  Intro
00:42  Welcome
01:12  Digitial Ocean AD
02:15  Bitwarden Ad
03:18  Interview with Philippe Humeau
27:10  Outro


